/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@gitzone/tsbuild',
  version: '2.1.66',
  description: 'TypeScript nightly to easily make use of latest features'
}
